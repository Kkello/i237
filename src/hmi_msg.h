#include <avr/pgmspace.h>
#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_

const char PROG_VER[] PROGMEM = "\nVersion: %s built on: %s %s";
const char LIBC_GCC_VER[] PROGMEM =
    "\navr-libc version: %s avr-gcc version: %s";
const char STUDENT[] PROGMEM = "Kkello";
const char ENTER[] PROGMEM = "\nEnter a letter>";
const char WRONG[] PROGMEM = "\nPlease enter a letter";
const char ENTERED[] PROGMEM = "\nYou entered: ";
const char LETTERS[24] PROGMEM = {"Januray", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December","januray", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december"
                                    };
#endif /* _HMI_MSG_H_ */
