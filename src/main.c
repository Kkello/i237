#include <avr/pgmspace.h>
#include <stdio.h>
#include <assert.h>
#include <avr/io.h>
#include <util/delay.h>
#include "uart.h"
#include "hmi_msg.h"
#include "print_helper.h"
#include "../lib/hd44780_111/hd44780.h"
#define BLINK_DELAY_MS 100


static inline void init_leds(void)
{
    /*Red pin 23, Blue pin 25, Green pin 27*/
    /* output: PORTB pin 7*/
    DDRB |= _BV(DDB7);
    /* output: PORTA pins 1,3,5*/
    DDRA |= _BV(DDB1);
    DDRA |= _BV(DDB3);
    DDRA |= _BV(DDB5);
    /*Turn off board LED*/
    PORTB &= ~_BV(PORTB7);
}


/* Init error console as stderr in UART1 and print user code info */
static inline void init_con(void)
{
    simple_uart0_init();
    stdin = stdout = &simple_uart0_io;
    fprintf_P(stdout, PROG_VER, FW_VERSION, __DATE__, __TIME__);
    fprintf_P(stdout, LIBC_GCC_VER, __AVR_LIBC_VERSION_STRING__, __VERSION__);
    fprintf(stdout, "\n");
    fprintf_P(stdout, STUDENT);
    fprintf(stdout, "\n");
}


static inline void blink_leds(void)
{
        /* Turn off Blue LED and turn on red LED */
        _delay_ms(BLINK_DELAY_MS);
        PORTA &= ~_BV(PORTB5);
        PORTA |= _BV(PORTB1);
        /* Turn off Red LED and turn on Green LED */
        _delay_ms(BLINK_DELAY_MS);
        PORTA &= ~_BV(PORTB1);
        PORTA |= _BV(PORTB3);
        /* Turn off Green LED and turn on Blue LED */
        _delay_ms(BLINK_DELAY_MS);
        PORTA &= ~_BV(PORTB3);
        PORTA |= _BV(PORTB5);
}




void main(void)
{
    //Initialise LEDs, console and LCD
    init_leds();
    init_con();
    lcd_init();
    lcd_clrscr();
    //Writes students name on the display
    lcd_puts_(STUDENT);
    //ASCII table values
    unsigned char array[128] = {0};-****7/4

    for (unsigned char i = 0; i < sizeof(array); i++) {
        array[i] = i;
    }

    //Print ASCII table and Print ASCII for humans using the array.
    fprintf(stdout, "\n");
    print_ascii_tbl(stdout);
    fprintf(stdout, "\n");
    print_for_human(stdout, array, sizeof(array) - 1);

    while (1) {
        //Prints text, reads input and prints input
        char input[24];
        fprintf_(stdout, ENTER);
        scanf("%s", input);
        fprintf(stdout, input);
        //char ch = atoi(input);

        //If input is between a and z or A and Z
        if((ch>='a' && ch<='z') || (ch>='A' && ch<='Z'))  {
            fprintf_(stdout, ENTERED);
            fprintf_(stdout, LETTERS[ch]);
        } else {
            fprintf_(stdout, WRONG);
        }

        //Blink LEDs
        blink_leds();
    }
}
